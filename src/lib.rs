use chrono::{Date, Utc};

use crate::domain::account::Account;
use crate::domain::date_generator_trait::DateGenerator;
use crate::infra::repository::statement_repository::StatementRepository;

pub mod domain;
pub mod infra;

pub struct DateGeneratorMain;

impl DateGenerator for DateGeneratorMain {
    fn today(&self) -> Date<Utc> {
        Utc::today()
    }
}

pub fn run() {
    println!("Hello World!");
    let date_generator = Box::new(DateGeneratorMain);
    let statement_repository = Box::new(StatementRepository::new());
    let mut account = Account::new(date_generator, statement_repository);

    account.deposit(70000);
    account.withdraw(500);
    account.withdraw(500);
    account.withdraw(100);
    account.withdraw(25);
    account.withdraw(475);
    account.withdraw(58588);
    account.withdraw(45);
    account.withdraw(10);
    account.withdraw(12);
    account.withdraw(5);

    println!("{}", account.print_statement());
}
