use crate::domain::transaction::Transaction;
use mockall::*;

#[automock]
pub trait StatementTrait{
    fn save_transaction(&mut self, transaction: Transaction);
    fn get_all_transaction(&mut self) -> Vec<Transaction>;
}
