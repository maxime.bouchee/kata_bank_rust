use crate::domain::date_generator_trait::DateGenerator;
use crate::domain::transaction::Transaction;
use crate::infra::repository::statement_trait::StatementTrait;

pub struct Account {
    pub balance: i32,
    date_generator: Box<dyn DateGenerator>,
    statement_repository: Box<dyn StatementTrait>,
}

impl Account {
    pub fn new(date_generator: Box<dyn DateGenerator>, statement_repository: Box<dyn StatementTrait>) -> Self {
        Self { balance: 0, date_generator, statement_repository }
    }
    pub fn deposit(&mut self, amount: i32) {
        if amount > 0 {
            self.balance += amount;
            self.statement_repository.save_transaction(Transaction::new(self.date_generator.today(), amount, self.balance));
        }
    }

    pub fn withdraw(&mut self, amount: i32) {
        self.balance -= amount;
        self.statement_repository.save_transaction(Transaction::new(self.date_generator.today(), -amount, self.balance));
    }

    pub fn print_statement(&mut self) -> String {
        let header = "date || credit || debit || balance".to_string();
        let row: Vec<String> = self.statement_repository
            .get_all_transaction()
            .iter()
            .rev()
            .map(format_transaction)
            .collect();
        header + "\n" + &row.join("\n")
    }
}

fn format_transaction(transaction: &Transaction) -> String {
    match transaction.amount < 0 {
        true => format!("{} || || {} || {}", transaction.date.format("%d/%m/%Y"), -transaction.amount, transaction.balance),
        false => format!("{} || {} || || {}", transaction.date.format("%d/%m/%Y"), transaction.amount, transaction.balance),
    }
}
