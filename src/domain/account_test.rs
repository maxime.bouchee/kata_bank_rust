#[cfg(test)]
mod tests {
    use chrono::{TimeZone, Utc};

    use crate::domain::account::Account;
    use crate::domain::date_generator_trait::MockDateGenerator;
    use crate::domain::transaction::Transaction;
    use crate::infra::repository::statement_trait::MockStatementTrait;

    #[test]
    fn account_is_created_with_balance_equal_0() {
        //GIVEN
        let mock_date_generator = Box::new(MockDateGenerator::new());
        let mock_statement = Box::new(MockStatementTrait::new());
        let account = Account::new(mock_date_generator, mock_statement);

        //WHEN / THEN
        assert_eq!(account.balance, 0);
    }

    #[test]
    fn deposit_500_add_500_to_balance() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mut mock_statement = Box::new(MockStatementTrait::new());
        mock_statement.expect_save_transaction().returning(|_t| ());
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.deposit(500);

        //THEN
        assert_eq!(account.balance, 500);
    }

    #[test]
    fn deposit_negative_value_do_nothing() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mock_statement = Box::new(MockStatementTrait::new());
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.deposit(-500);

        //THEN
        assert_eq!(account.balance, 0);
    }

    #[test]
    fn withdraw_500_remove_500_to_balance() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mut mock_statement = Box::new(MockStatementTrait::new());
        mock_statement.expect_save_transaction().returning(|_t| ());
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.withdraw(500);

        //THEN
        assert_eq!(account.balance, -500);
    }

    #[test]
    fn print_statement_print_all_operation_on_the_account_when_deposit_500() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mut mock_statement = Box::new(MockStatementTrait::new());
        mock_statement.expect_save_transaction().returning(|_t| ());
        let expected_transactions = vec![Transaction::new(Utc.ymd(2012, 01, 14), 500, 500)];
        mock_statement.expect_get_all_transaction().return_const(expected_transactions);
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.deposit(500);

        //THEN
        assert_eq!(account.print_statement(),
                   "date || credit || debit || balance\
                   \n14/01/2012 || 500 || || 500"
        );
    }

    #[test]
    fn print_statement_print_all_operation_on_the_account_when_withdraw_500() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mut mock_statement = Box::new(MockStatementTrait::new());
        mock_statement.expect_save_transaction().returning(|_t| ());
        let expected_transactions = vec![Transaction::new(Utc.ymd(2012, 01, 14), -500, -500)];
        mock_statement.expect_get_all_transaction().return_const(expected_transactions);
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.withdraw(500);

        //THEN
        assert_eq!(account.print_statement(),
                   "date || credit || debit || balance\
                   \n14/01/2012 || || 500 || -500"
        );
    }

    #[test]
    fn print_statement_print_all_operation_on_the_account() {
        //GIVEN
        let mut mock_date_generator = Box::new(MockDateGenerator::new());
        let mut mock_statement = Box::new(MockStatementTrait::new());
        mock_statement.expect_save_transaction().returning(|_t| ());

        let expected_transactions = vec![
            Transaction::new(Utc.ymd(2012, 01, 14), 2500, 2500),
            Transaction::new(Utc.ymd(2012, 01, 14), -125, 2375),
            Transaction::new(Utc.ymd(2012, 01, 14), -600, 1775),
        ];

        mock_statement.expect_get_all_transaction().return_const(expected_transactions);
        mock_date_generator.expect_today().returning(|| Utc.ymd(2012, 01, 14));
        let mut account = Account::new(mock_date_generator, mock_statement);

        //WHEN
        account.deposit(2500);
        account.withdraw(125);
        account.withdraw(600);

        //THEN
        assert_eq!(account.print_statement(),
                   "date || credit || debit || balance\
                   \n14/01/2012 || || 600 || 1775\
                   \n14/01/2012 || || 125 || 2375\
                   \n14/01/2012 || 2500 || || 2500"
        );
    }
}
