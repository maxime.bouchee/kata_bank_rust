use crate::domain::transaction::Transaction;
use crate::infra::repository::statement_trait::StatementTrait;

pub struct StatementRepository {
    statement: Vec<Transaction>,
}

impl StatementRepository{
    pub fn new() -> Self {
        Self { statement: Vec::new() }
    }
}

impl StatementTrait for StatementRepository {
    fn save_transaction(&mut self, transaction: Transaction) {
        self.statement.push(transaction);
    }
    fn get_all_transaction(&mut self) -> Vec<Transaction> {
        self.statement.to_vec()
    }
}
