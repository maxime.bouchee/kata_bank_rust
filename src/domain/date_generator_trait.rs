use chrono::{Date, Utc};
use mockall::*;

#[automock]
pub trait DateGenerator {
    fn today(&self) -> Date<Utc>;
}
