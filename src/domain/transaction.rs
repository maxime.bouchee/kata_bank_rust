use chrono::{Date, Utc};

#[derive(Clone)]
pub struct Transaction {
    pub date: Date<Utc>,
    pub amount: i32,
    pub balance: i32,
}

impl Transaction {
    pub fn new(date_of_the_day: Date<Utc>, amount: i32, balance: i32) -> Self {
        Self { date: date_of_the_day, amount, balance }
    }
}

